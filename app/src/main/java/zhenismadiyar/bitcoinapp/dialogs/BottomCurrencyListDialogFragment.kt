package zhenismadiyar.bitcoinapp.dialogs

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.bottom_sheet_currency_list.*
import zhenismadiyar.bitcoinapp.R
import zhenismadiyar.bitcoinapp.callbacks.Callback
import zhenismadiyar.bitcoinapp.helpers.RoundedBottomSheetDialogFragment

/**
 * Show list of currencies on bottom sheet dialog (supported values KZT, USD, EUR)
 * @author Zhenis Madiyar
 */
class BottomCurrencyListDialogFragment : RoundedBottomSheetDialogFragment(), View.OnClickListener {

    lateinit var onCurrencySelectedCallback: Callback
    fun onCurrencySelected(callback: Callback) {
        onCurrencySelectedCallback = callback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_currency_list, container, false)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val bundle = this.arguments
        if (bundle != null) {
            val selectedCurrency = bundle.getString("selectedCurrency")

            when (selectedCurrency) {

                getString(R.string.eur) -> {
                    buttonEUR.isClickable = false
                    buttonEUR.isEnabled = false
                }
                getString(R.string.usd) -> {
                    buttonUSD.isClickable = false
                    buttonUSD.isEnabled = false
                }
                getString(R.string.kzt) -> {
                    buttonKZT.isClickable = false
                    buttonKZT.isEnabled = false
                }
            }
        }
        clickListeners()
    }

    private fun clickListeners() {

        buttonUSD.setOnClickListener(this)
        buttonKZT.setOnClickListener(this)
        buttonEUR.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.buttonEUR -> {
                onCurrencySelectedCallback.process("EUR")
            }
            R.id.buttonUSD -> {
                onCurrencySelectedCallback.process("USD")
            }
            R.id.buttonKZT -> {
                onCurrencySelectedCallback.process("KZT")
            }
        }
    }

}