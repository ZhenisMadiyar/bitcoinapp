package zhenismadiyar.bitcoinapp.mvp.presenters

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.arellomobile.mvp.InjectViewState
import com.github.mikephil.charting.data.Entry
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONException
import org.json.JSONObject
import zhenismadiyar.bitcoinapp.BitcoinApplication
import zhenismadiyar.bitcoinapp.api.BitcoinApiService
import zhenismadiyar.bitcoinapp.dagger.components.DaggerServiceComponent
import zhenismadiyar.bitcoinapp.helpers.PreferenceHelper
import zhenismadiyar.bitcoinapp.mvp.views.interfaces.HomeView
import zhenismadiyar.bitcoinapp.utils.MyLogger
import java.util.*
import javax.inject.Inject
import java.text.SimpleDateFormat


@InjectViewState
class HomePresenter @Inject constructor() : BasePresenter<HomeView>() {

    init {
        DaggerServiceComponent.builder()
            .appComponent(BitcoinApplication.getApplicationComponent())
            .build().inject(this)
    }

    @Inject
    lateinit var apiService: BitcoinApiService

    /**
     * get currency current BTC value
     */
    fun getCurrencyBitcoinValue(currency: String) {

        disposables.add(
            apiService.getBtcCurrencyPrice(currency)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    PreferenceHelper.defaultPrefs(BitcoinApplication.getApplicationComponent().getContext()).edit().putString(BitcoinApiService.LAST_CURRENCY, currency).apply()
                    viewState.onLoadChartData(it, currency)
                }, {
                    MyLogger.e("Error getCurrencyBitcoinValue ${it.cause}")
                })
        )
    }

    @SuppressLint("SimpleDateFormat")
    fun getHistoryBtc(currency: String, period: String) {

        var api = apiService.getBtcCurrencyMonthHistory(currency)
        when (period) {
            "week" -> {
                /**
                 * get current week first date and last date
                 */
                val dateFormat = SimpleDateFormat("yyyy-MM-dd")
                val cal = Calendar.getInstance()
                cal.set(Calendar.HOUR_OF_DAY, 0) // ! clear would not reset the hour of day !
                cal.clear(Calendar.MINUTE)
                cal.clear(Calendar.SECOND)
                cal.clear(Calendar.MILLISECOND)

                // get start of this week in milliseconds
                cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)
                val startDate = dateFormat.format(cal.time)
                // start of the next week
                cal.add(Calendar.WEEK_OF_YEAR, 1)
                val endDate = dateFormat.format(cal.time)

                api = apiService.getBtcCurrencyHistory(currency, startDate, endDate)
            }
        }

        disposables.add(
            api.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    val bpi = it.get("bpi") as JsonObject
                    val jsonValue = JSONObject(bpi.toString())
                    val iter = jsonValue.keys()

                    val entries = ArrayList<Entry>()

                    var x = 1

                    while (iter.hasNext()) {
                        val key = iter.next()
                        try {
                            val value = jsonValue.get(key)
                            MyLogger.i(value.toString())

                            entries.add(Entry(x.toFloat(), value.toString().toFloat()))
                            x += 1
                        } catch (e: JSONException) {
                            // Something went wrong!
                            MyLogger.e("Error getBtcCurrencyHistory inside while ${it.toString()}")
                        }

                    }
                    viewState.onLoadChartEntry(entries)
                }, {
                    MyLogger.e("Error getBtcCurrencyHistory ${it.toString()}")
                })
        )
    }
}