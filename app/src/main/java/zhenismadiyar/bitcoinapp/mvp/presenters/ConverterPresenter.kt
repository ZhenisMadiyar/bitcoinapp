package zhenismadiyar.bitcoinapp.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import zhenismadiyar.bitcoinapp.BitcoinApplication
import zhenismadiyar.bitcoinapp.api.BitcoinApiService
import zhenismadiyar.bitcoinapp.dagger.components.DaggerServiceComponent
import zhenismadiyar.bitcoinapp.mvp.views.interfaces.ConverterView
import zhenismadiyar.bitcoinapp.mvp.views.interfaces.TransactionListView
import javax.inject.Inject

@InjectViewState
class ConverterPresenter @Inject constructor() : BasePresenter<ConverterView>() {

    init {
        DaggerServiceComponent.builder()
            .appComponent(BitcoinApplication.getApplicationComponent())
            .build().inject(this)
    }

    @Inject
    lateinit var apiService: BitcoinApiService

    /**
     * get currency current btc value
     */
    fun getCurrencyValue(currency: String) {

        disposables.add(
            apiService.getBtcCurrencyPrice(currency)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    when (currency) {
                        "KZT" -> {
                            viewState.onLoadCurrencyValue(it.bpi.kzt.rateFloat)
                        }
                        "USD" -> {
                            viewState.onLoadCurrencyValue(it.bpi.usd.rateFloat)
                        }
                        "EUR" -> {
                            viewState.onLoadCurrencyValue(it.bpi.eur.rateFloat)
                        }
                    }
                }, {
                    viewState.errorGetData(it.message.toString())
                })
        )
    }
}