package zhenismadiyar.bitcoinapp.mvp.presenters

import com.arellomobile.mvp.InjectViewState
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import zhenismadiyar.bitcoinapp.BitcoinApplication
import zhenismadiyar.bitcoinapp.api.BitcoinApiService
import zhenismadiyar.bitcoinapp.dagger.components.DaggerServiceComponent
import zhenismadiyar.bitcoinapp.mvp.models.TransactionListModel
import zhenismadiyar.bitcoinapp.mvp.views.interfaces.TransactionListView
import javax.inject.Inject

@InjectViewState
class TransactionListPresenter @Inject constructor() : BasePresenter<TransactionListView>() {

    init {
        DaggerServiceComponent.builder()
            .appComponent(BitcoinApplication.getApplicationComponent())
            .build().inject(this)
    }

    @Inject
    lateinit var apiService: BitcoinApiService

    fun getTransactionList() {

        disposables.add(
            apiService.getTransactionsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it ->

                    val listTransaction = ArrayList<TransactionListModel>()
                    it.forEach {
                        val item =
                            Gson().fromJson<TransactionListModel>(it.toString(), TransactionListModel::class.java)
                        listTransaction.add(item)
                    }
                    viewState.onLoadTransactionListData(listTransaction)

                }, {
                    viewState.errorGetTransactionListData(it.message.toString())
                })
        )

    }
}