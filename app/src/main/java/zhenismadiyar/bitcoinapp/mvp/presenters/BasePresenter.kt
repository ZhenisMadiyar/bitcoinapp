package zhenismadiyar.bitcoinapp.mvp.presenters

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import zhenismadiyar.bitcoinapp.BitcoinApplication

open class BasePresenter<T : MvpView> : MvpPresenter<T>() {

    val disposables = CompositeDisposable()
    private val apiService = BitcoinApplication.serviceComponent.getApiService()
    private val prefs = BitcoinApplication.appComponent.getPrefs()


}