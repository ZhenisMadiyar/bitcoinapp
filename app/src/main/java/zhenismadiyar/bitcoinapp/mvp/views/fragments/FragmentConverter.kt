package zhenismadiyar.bitcoinapp.mvp.views.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.fragment_converter.*
import org.jetbrains.anko.support.v4.toast
import zhenismadiyar.bitcoinapp.R
import zhenismadiyar.bitcoinapp.api.BitcoinApiService.Companion.LAST_CONVERTER_CURRENCY
import zhenismadiyar.bitcoinapp.callbacks.Callback
import zhenismadiyar.bitcoinapp.dialogs.BottomCurrencyListDialogFragment
import zhenismadiyar.bitcoinapp.helpers.PreferenceHelper
import zhenismadiyar.bitcoinapp.mvp.presenters.ConverterPresenter
import zhenismadiyar.bitcoinapp.mvp.views.interfaces.ConverterView
import java.text.NumberFormat

class FragmentConverter : MvpAppCompatFragment(), ConverterView, View.OnClickListener {

    @InjectPresenter
    lateinit var presenter: ConverterPresenter
    private var selectedCurrency = ""
    private var btcValue: Double = 0.0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_converter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        //get las used currency
        selectedCurrency = PreferenceHelper.defaultPrefs(activity!!).getString(LAST_CONVERTER_CURRENCY, "KZT")!!

        clickListeners()
        setCurrencyImage(selectedCurrency)

        //get last value of currency
        presenter.getCurrencyValue(selectedCurrency)

        editTextFragmentConvert.addTextChangedListener(object : TextWatcher {

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable?) {
                if (s != null && !s.isEmpty()) {
                    val editTextValue = s.toString()
                    if (btcValue != 0.0)
                        textViewFragmentConvertValue.text = NumberFormat.getInstance().format(Math.round(editTextValue.toDouble() * btcValue)).toString()+" $selectedCurrency"
                } else {
                    textViewFragmentConvertValue.text = ""
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }

    /**
     * set currency image icon
     */
    private fun setCurrencyImage(currency: String) {
        when (currency) {
            "KZT" -> {
                imageViewFragmentConverterFlag.setImageResource(R.drawable.ic_kazakhstan)
            }
            "EUR" -> {
                imageViewFragmentConverterFlag.setImageResource(R.drawable.ic_european_union)
            }
            "USD" -> {
                imageViewFragmentConverterFlag.setImageResource(R.drawable.ic_united_states)
            }
        }
    }

    private fun clickListeners() {
        imageViewFragmentConverterFlag.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.imageViewFragmentConverterFlag -> {

                val bundle = Bundle()
                bundle.putSerializable("selectedCurrency", selectedCurrency)

                val bottomPlaySheetDialog = BottomCurrencyListDialogFragment()
                bottomPlaySheetDialog.arguments = bundle
                bottomPlaySheetDialog.show(childFragmentManager, "CurrencyListFragment")
                bottomPlaySheetDialog.onCurrencySelected(Callback {

                    val currency = it as String
                    selectedCurrency = currency

                    //save selected currency value
                    PreferenceHelper.defaultPrefs(activity!!).edit().putString(LAST_CONVERTER_CURRENCY, currency).apply()

                    //get new btc value
                    presenter.getCurrencyValue(currency)

                    //clear text value
                    textViewFragmentConvertValue.text = ""
                    editTextFragmentConvert.setText("")

                    //change currency icon
                    when (currency) {
                        "KZT" -> {
                            imageViewFragmentConverterFlag.setImageResource(R.drawable.ic_kazakhstan)
                        }
                        "EUR" -> {
                            imageViewFragmentConverterFlag.setImageResource(R.drawable.ic_european_union)
                        }
                        "USD" -> {
                            imageViewFragmentConverterFlag.setImageResource(R.drawable.ic_united_states)
                        }
                    }

                    bottomPlaySheetDialog.dismiss()
                })
            }
        }
    }

    /**
     * load(set) currency current btc value
     */
    override fun onLoadCurrencyValue(btcValue: Double) {
        this.btcValue = btcValue
    }

    override fun errorGetData(msg: String?) {
        if (msg != null)
            toast(msg)
    }
}