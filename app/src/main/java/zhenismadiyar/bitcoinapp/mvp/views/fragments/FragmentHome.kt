package zhenismadiyar.bitcoinapp.mvp.views.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.support.v4.toast
import zhenismadiyar.bitcoinapp.R
import zhenismadiyar.bitcoinapp.api.BitcoinApiService.Companion.LAST_CURRENCY
import zhenismadiyar.bitcoinapp.callbacks.Callback
import zhenismadiyar.bitcoinapp.dialogs.BottomCurrencyListDialogFragment
import zhenismadiyar.bitcoinapp.helpers.PreferenceHelper
import zhenismadiyar.bitcoinapp.mvp.models.BitcoinCurrencyModel
import zhenismadiyar.bitcoinapp.mvp.presenters.HomePresenter
import zhenismadiyar.bitcoinapp.mvp.views.interfaces.HomeView
import zhenismadiyar.bitcoinapp.utils.MyLogger
import java.util.*

class FragmentHome : MvpAppCompatFragment(), HomeView, View.OnClickListener {

    @InjectPresenter
    lateinit var presenter: HomePresenter
    private var selectedCurrency: String = ""
    private var isMonth = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        selectedCurrency = PreferenceHelper.defaultPrefs(activity!!).getString(LAST_CURRENCY, "KZT")!!
        radioButtonForMonth.isChecked = true

        presenter.getCurrencyBitcoinValue(selectedCurrency)
        presenter.getHistoryBtc(selectedCurrency, "month")

        clickListeners()
        radioButtonCheckListeners()

        //until all data is loaded, set container view gone
        swipeRefreshLayoutFragmentHome.isRefreshing = true
        containerFragmentHome.visibility = View.GONE
        buttonHomeFragmentChangeCurrency.visibility = View.GONE

        swipeRefreshLayoutFragmentHome.setOnRefreshListener {
            presenter.getCurrencyBitcoinValue(selectedCurrency)
            if (isMonth)
                presenter.getHistoryBtc(selectedCurrency, "month")
            else
                presenter.getHistoryBtc(selectedCurrency, "week")
        }
    }

    private fun radioButtonCheckListeners() {
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radioButtonForMonth -> {
                    isMonth = true
                    presenter.getHistoryBtc(selectedCurrency, "month")
                }
                R.id.radioButtonForWeek -> {
                    isMonth = false
                    presenter.getHistoryBtc(selectedCurrency, "week")
                }
            }
        }
    }

    private fun clickListeners() {
        buttonHomeFragmentChangeCurrency.setOnClickListener(this)
    }

    /**
     * get currency data (description, title, code, rate)
     */
    @SuppressLint("SetTextI18n")
    override fun onLoadChartData(result: BitcoinCurrencyModel, currentCurrency: String) {

        val currencyFullTitle: String
        val currencyCode: String
        val currencyRate: String

        when (currentCurrency) {
            "KZT" -> {
                currencyFullTitle = result.bpi.kzt.description
                currencyCode = result.bpi.kzt.code
                currencyRate = result.bpi.kzt.rate
            }
            "EUR" -> {
                currencyFullTitle = result.bpi.eur.description
                currencyCode = result.bpi.eur.code
                currencyRate = result.bpi.eur.rate
            }
            else -> {
                currencyFullTitle = result.bpi.usd.description
                currencyCode = result.bpi.usd.code
                currencyRate = result.bpi.usd.rate
            }
        }

        //when all data is loaded, set view visible
        swipeRefreshLayoutFragmentHome.isRefreshing = false
        containerFragmentHome.visibility = View.VISIBLE
        buttonHomeFragmentChangeCurrency.visibility = View.VISIBLE

        textViewHomeFragmentSelectedCurrencyTitle.text = currencyFullTitle
        textViewHomeFragmentBtcValueSelectedCurrency.text = currencyRate
        textViewHomeFragmentSelectedCurrencyCode.text = "BTC/$currencyCode"
        textViewHomeFragmentBtcValueSelectedCurrencySymbol.text = currencyCode
    }

    /**
     * get data coordinates and draw charts
     */
    override fun onLoadChartEntry(listEntry: ArrayList<Entry>) {

        listEntry.forEach {
            MyLogger.i(it.x.toString() + ", ${it.y}")
        }

        val dataSet = LineDataSet(listEntry, "")
        val lineData = LineData(dataSet)

        chart.description.text = ""
        chart.data = lineData
        chart.invalidate() // refresh
    }

    override fun onClick(v: View?) {

        when (v?.id) {

            /**
             * Open bottomsheet dialog, list of currency
             */
            R.id.buttonHomeFragmentChangeCurrency -> {

                val bundle = Bundle()
                bundle.putSerializable("selectedCurrency", selectedCurrency)

                val bottomPlaySheetDialog = BottomCurrencyListDialogFragment()
                bottomPlaySheetDialog.arguments = bundle
                bottomPlaySheetDialog.show(childFragmentManager, "Currency List Bottom Sheet")
                bottomPlaySheetDialog.onCurrencySelected(Callback {

                    val currency = it as String
                    selectedCurrency = currency
                    presenter.getCurrencyBitcoinValue(currency)
                    if (isMonth)
                        presenter.getHistoryBtc(currency, "month")
                    else
                        presenter.getHistoryBtc(currency, "week")
                    bottomPlaySheetDialog.dismiss()
                })
            }
        }
    }

    /**
     * get data error
     */
    override fun errorGetChartData() {
        swipeRefreshLayoutFragmentHome.isRefreshing = false
        toast("Произошла ошибка. Попробуйте позже")
    }
}