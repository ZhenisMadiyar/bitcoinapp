package zhenismadiyar.bitcoinapp.mvp.views.interfaces

import com.arellomobile.mvp.MvpView
import com.github.mikephil.charting.data.Entry
import zhenismadiyar.bitcoinapp.mvp.models.BitcoinCurrencyModel

interface HomeView: MvpView {

    fun onLoadChartData(result: BitcoinCurrencyModel, currentCurrency: String)
    fun errorGetChartData()

    fun onLoadChartEntry(listEntry: ArrayList<Entry>)
}