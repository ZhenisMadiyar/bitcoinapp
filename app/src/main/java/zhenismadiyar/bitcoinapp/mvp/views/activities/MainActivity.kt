package zhenismadiyar.bitcoinapp.mvp.views.activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentContainer
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.viven.fragmentstatemanager.FragmentStateManager
import kotlinx.android.synthetic.main.activity_main.*
import me.jessyan.retrofiturlmanager.RetrofitUrlManager
import zhenismadiyar.bitcoinapp.R
import zhenismadiyar.bitcoinapp.api.BitcoinApiService.Companion.transactionUrl
import zhenismadiyar.bitcoinapp.mvp.views.fragments.FragmentConverter
import zhenismadiyar.bitcoinapp.mvp.views.fragments.FragmentHome
import zhenismadiyar.bitcoinapp.mvp.views.fragments.FragmentTransactionList

class MainActivity : AppCompatActivity() {

    private var fragmentStateManager: FragmentStateManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentStateManager = object : FragmentStateManager(content, supportFragmentManager) {
            override fun getItem(position: Int): Fragment {
                return when (position) {
                    0 -> FragmentHome()
                    1 -> FragmentTransactionList()
                    2 -> FragmentConverter()
                    else -> FragmentHome()
                }
            }
        }
        //set default first screen
        fragmentStateManager?.changeFragment(0)
        navigation.selectedItemId = R.id.navigation_home
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.setOnNavigationItemReselectedListener(mOnNavigationItemReselectedListener)

        /**
         * @see FragmentTransactionList for transaction fragment's retrofit url
         */
        RetrofitUrlManager.getInstance().putDomain("douban", transactionUrl)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val position = getNavPositionFromMenuItem(item)
        if (position != -1) {
            fragmentStateManager?.changeFragment(getNavPositionFromMenuItem(item))
            return@OnNavigationItemSelectedListener true
        }

        return@OnNavigationItemSelectedListener false
    }

    private val mOnNavigationItemReselectedListener = BottomNavigationView.OnNavigationItemReselectedListener { item ->
        val position = getNavPositionFromMenuItem(item)
        if (position != -1) {
            fragmentStateManager?.removeFragment(position)
            fragmentStateManager?.changeFragment(position)
        }
    }

    private fun getNavPositionFromMenuItem(menuItem: MenuItem): Int {
        return when (menuItem.itemId) {
            R.id.navigation_home -> 0
            R.id.navigation_transaction_list -> 1
            R.id.navigation_converter -> 2
            else -> -1
        }
    }
}
