package zhenismadiyar.bitcoinapp.mvp.views.interfaces

import com.arellomobile.mvp.MvpView
import com.github.mikephil.charting.data.Entry
import zhenismadiyar.bitcoinapp.mvp.models.BitcoinCurrencyModel
import zhenismadiyar.bitcoinapp.mvp.models.TransactionListModel

interface TransactionListView: MvpView {

    fun onLoadTransactionListData(listTransactionList: List<TransactionListModel>)
    fun errorGetTransactionListData(msg:String)
}