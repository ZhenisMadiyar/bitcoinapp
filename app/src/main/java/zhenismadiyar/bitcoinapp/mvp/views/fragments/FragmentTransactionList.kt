package zhenismadiyar.bitcoinapp.mvp.views.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.fragment_transaction_list.*
import org.jetbrains.anko.support.v4.toast
import zhenismadiyar.bitcoinapp.R
import zhenismadiyar.bitcoinapp.adapters.TransactionListAdapter
import zhenismadiyar.bitcoinapp.callbacks.ItemClickListener
import zhenismadiyar.bitcoinapp.mvp.models.TransactionListModel
import zhenismadiyar.bitcoinapp.mvp.presenters.TransactionListPresenter
import zhenismadiyar.bitcoinapp.mvp.views.interfaces.TransactionListView

class FragmentTransactionList : MvpAppCompatFragment(), TransactionListView {

    @InjectPresenter
    lateinit var presenter: TransactionListPresenter

    var adapter: TransactionListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_transaction_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        swipeRefreshLayoutTransactionList.isRefreshing = true
        presenter.getTransactionList()
        initRVTransactionList()

        swipeRefreshLayoutTransactionList.setOnRefreshListener {
            presenter.getTransactionList()
        }
    }

    /**
     * init RV transaction list
     */
    private fun initRVTransactionList() {

        val layManager = LinearLayoutManager(activity)
        recyclerViewTransactionList.layoutManager = layManager

        adapter = TransactionListAdapter()
        recyclerViewTransactionList.adapter = adapter

        adapter?.setOnClickListener(ItemClickListener { position, _list ->

            val item = _list as TransactionListModel
            try {
                toast(item.tid.toString())
            } catch (e: Exception) {}
        })
    }

    /**
     *@see TransactionListAdapter get list of Transactions, and notify adapter
     */
    override fun onLoadTransactionListData(listTransactionList: List<TransactionListModel>) {
        swipeRefreshLayoutTransactionList.isRefreshing = false
        adapter?.addListItems(listTransactionList)
    }

    /**
     * failed to load transaction list
     */
    override fun errorGetTransactionListData(msg: String) {
        swipeRefreshLayoutTransactionList.isRefreshing = false
        toast("Произошла ошибка. Попробуйте позже. $msg")
    }
}