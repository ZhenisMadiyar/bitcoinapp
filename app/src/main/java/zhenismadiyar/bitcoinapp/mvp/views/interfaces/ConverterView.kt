package zhenismadiyar.bitcoinapp.mvp.views.interfaces

import com.arellomobile.mvp.MvpView
import com.github.mikephil.charting.data.Entry
import zhenismadiyar.bitcoinapp.mvp.models.BitcoinCurrencyModel

interface ConverterView : MvpView {

    fun onLoadCurrencyValue(btcValue: Double)
    fun errorGetData(msg: String?)
}