package zhenismadiyar.bitcoinapp.mvp.models

import com.google.gson.annotations.SerializedName

data class TransactionListModel(
    @SerializedName("date")
    val date: String = "",
    @SerializedName("amount")
    val amount: String = "",
    @SerializedName("price")
    val price: String = "",
    @SerializedName("type")
    val type: Int = 0,
    @SerializedName("tid")
    val tid: Int = 0
)