package zhenismadiyar.bitcoinapp.mvp.models


import com.google.gson.annotations.SerializedName


data class BitcoinCurrencyModel(
    @SerializedName("bpi")
    val bpi: Bpi,
    @SerializedName("time")
    val time: Time,
    @SerializedName("disclaimer")
    val disclaimer: String = ""
)

data class Bpi(
    @SerializedName("KZT")
    val kzt: KZT,
    @SerializedName("USD")
    val usd: USD,
    @SerializedName("EUR")
    val eur: EUR
)

data class KZT(
    @SerializedName("rate_float")
    val rateFloat: Double = 0.0,
    @SerializedName("code")
    val code: String = "",
    @SerializedName("rate")
    val rate: String = "",
    @SerializedName("description")
    val description: String = ""
)


data class USD(
    @SerializedName("rate_float")
    val rateFloat: Double = 0.0,
    @SerializedName("code")
    val code: String = "",
    @SerializedName("rate")
    val rate: String = "",
    @SerializedName("description")
    val description: String = ""
)

data class EUR(
    @SerializedName("rate_float")
    val rateFloat: Double = 0.0,
    @SerializedName("code")
    val code: String = "",
    @SerializedName("rate")
    val rate: String = "",
    @SerializedName("description")
    val description: String = ""
)


data class Time(
    @SerializedName("updateduk")
    val updateduk: String = "",
    @SerializedName("updatedISO")
    val updatedISO: String = "",
    @SerializedName("updated")
    val updated: String = ""
)


