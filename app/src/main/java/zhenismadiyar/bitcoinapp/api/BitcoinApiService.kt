package zhenismadiyar.bitcoinapp.api

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.*
import zhenismadiyar.bitcoinapp.mvp.models.BitcoinCurrencyModel

interface BitcoinApiService {

    companion object {

        const val baseUrl = "https://api.coindesk.com/"
        const val transactionUrl = "https://www.bitstamp.net/"

        const val LAST_CURRENCY = "get_last_currency"
        const val LAST_CONVERTER_CURRENCY = "get_last_converter_currency"
    }

    @GET("/v1/bpi/currentprice/{currency_code}.json")
    fun getBtcCurrencyPrice(@Path("currency_code") currency_code: String): Observable<BitcoinCurrencyModel>

    @GET("/v1/bpi/historical/close.json")
    fun getBtcCurrencyHistory(
        @Query("currency") currency: String,
        @Query("start") start: String,
        @Query("end") end: String
    ): Observable<JsonObject>

    @GET("/v1/bpi/historical/close.json")
    fun getBtcCurrencyMonthHistory(@Query("currency") currency: String): Observable<JsonObject>

    //for transaction list api
    @Headers("Domain-Name: douban") // Add the Domain-Name header
    @POST("/api/transactions")
    fun getTransactionsList(): Observable<JsonArray>
}
