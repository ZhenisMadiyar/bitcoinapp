package zhenismadiyar.bitcoinapp.callbacks;

public interface ItemClickListener {
    void onListClick(int position, Object _list);
}