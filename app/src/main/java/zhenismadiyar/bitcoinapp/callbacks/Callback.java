package zhenismadiyar.bitcoinapp.callbacks;

public interface Callback {
    void process(Object o);
}
