package zhenismadiyar.bitcoinapp.helpers

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import zhenismadiyar.bitcoinapp.R

open class RoundedBottomSheetDialogFragment : MvpAppCompatDialogFragment() {

    override fun getTheme(): Int = R.style.BaseBottomSheetDialogLight

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = BottomSheetDialog(requireContext(), theme)

}