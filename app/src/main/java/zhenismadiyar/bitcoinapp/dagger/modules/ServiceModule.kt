package zhenismadiyar.bitcoinapp.dagger.modules

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import zhenismadiyar.bitcoinapp.api.BitcoinApiService
import zhenismadiyar.bitcoinapp.dagger.scopes.PerActivity

@Module
class ServiceModule {

    @PerActivity
    @Provides
    fun getApiService(retrofit: Retrofit): BitcoinApiService {
        return retrofit.create(BitcoinApiService::class.java)
    }
}