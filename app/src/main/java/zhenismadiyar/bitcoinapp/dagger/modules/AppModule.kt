package zhenismadiyar.bitcoinapp.dagger.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import zhenismadiyar.bitcoinapp.dagger.scopes.AppScope

@Module
class AppModule {

    var context: Context
    var baseUrl: String

    constructor(context: Context, baseUrl: String) {
        this.context = context
        this.baseUrl = baseUrl
    }

    @AppScope
    @Provides
    fun context(): Context {
        return context
    }

    @AppScope
    @Provides
    fun retrofit(okHttpClient: OkHttpClient, provideRxAdapter: RxJava2CallAdapterFactory): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(provideRxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build()
    }

    @AppScope
    @Provides
    fun getSharedPrefs(context: Context): SharedPreferences {
        return context.getSharedPreferences("settings", Context.MODE_PRIVATE)
    }

    @AppScope
    @Provides
    fun provideRxAdapter(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }
}