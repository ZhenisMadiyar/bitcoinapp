package zhenismadiyar.bitcoinapp.dagger.components

import android.content.Context
import android.content.SharedPreferences
import dagger.Component
import retrofit2.Retrofit
import zhenismadiyar.bitcoinapp.dagger.modules.AppModule
import zhenismadiyar.bitcoinapp.dagger.modules.NetworkModule
import zhenismadiyar.bitcoinapp.dagger.scopes.AppScope

@AppScope
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {

    fun getContext(): Context
    fun getRetrofit(): Retrofit
    fun getPrefs(): SharedPreferences
}