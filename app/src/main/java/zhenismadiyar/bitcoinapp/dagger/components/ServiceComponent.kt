package zhenismadiyar.bitcoinapp.dagger.components

import dagger.Component
import zhenismadiyar.bitcoinapp.api.BitcoinApiService
import zhenismadiyar.bitcoinapp.dagger.modules.ServiceModule
import zhenismadiyar.bitcoinapp.dagger.scopes.PerActivity
import zhenismadiyar.bitcoinapp.mvp.presenters.ConverterPresenter
import zhenismadiyar.bitcoinapp.mvp.presenters.HomePresenter
import zhenismadiyar.bitcoinapp.mvp.presenters.TransactionListPresenter

@PerActivity
@Component(modules = [ServiceModule::class], dependencies = [AppComponent::class])
interface ServiceComponent {

    fun inject(presenter: HomePresenter)
    fun inject(presenter: TransactionListPresenter)
    fun inject(presenter: ConverterPresenter)

    fun getApiService(): BitcoinApiService
}