package zhenismadiyar.bitcoinapp

import android.app.Activity
import android.app.Application
import zhenismadiyar.bitcoinapp.api.BitcoinApiService
import zhenismadiyar.bitcoinapp.dagger.components.AppComponent
import zhenismadiyar.bitcoinapp.dagger.components.DaggerAppComponent
import zhenismadiyar.bitcoinapp.dagger.components.DaggerServiceComponent
import zhenismadiyar.bitcoinapp.dagger.components.ServiceComponent
import zhenismadiyar.bitcoinapp.dagger.modules.AppModule

class BitcoinApplication : Application() {

    companion object {

        fun get(activity: Activity): BitcoinApplication {
            return activity.application as BitcoinApplication
        }

        lateinit var appComponent: AppComponent
        lateinit var serviceComponent: ServiceComponent

        fun getApplicationComponent(): AppComponent {
            return appComponent
        }
    }

    override fun onCreate() {
        super.onCreate()

        createAppComponent()
        createServiceComponent()
    }

    private fun createAppComponent() {
        appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this, BitcoinApiService.baseUrl))
            .build()
    }

    private fun createServiceComponent() {
        serviceComponent = DaggerServiceComponent
            .builder()
            .appComponent(appComponent)
            .build()
    }

}