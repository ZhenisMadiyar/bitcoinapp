package zhenismadiyar.bitcoinapp.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row_transaction_item.view.*
import org.jetbrains.anko.textColor
import zhenismadiyar.bitcoinapp.R
import zhenismadiyar.bitcoinapp.callbacks.ItemClickListener
import zhenismadiyar.bitcoinapp.mvp.models.TransactionListModel
import zhenismadiyar.bitcoinapp.utils.MyLogger

/**
 * Created by Zhenis Madiyar on 03.01.2019.
 */

class TransactionListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val lists: MutableList<TransactionListModel>?
    var selected_position = 0

    init {
        lists = ArrayList()
    }

    var mItemClickListener: ItemClickListener? = null

    fun setOnClickListener(click: ItemClickListener) {
        mItemClickListener = click
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {

            if (lists != null)
                if (lists[position] != null) {
                    (holder as ViewHolder).bindData(lists[position], position)
                }
        } catch (e: Exception) {
            MyLogger.e("Error bindviewholder ${e.toString()}")
        }
    }

    fun addListItems(transactionList: List<TransactionListModel>) {
        lists?.addAll(transactionList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        if (lists == null)
            return 0

        return lists.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_transaction_item, parent, false)
        return ViewHolder(itemView)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n", "SimpleDateFormat")
        fun bindData(adapterItem: TransactionListModel, position: Int) {


            // convert seconds to milliseconds
            val date = java.util.Date(adapterItem.date.toLong() * 1000L)
            // the format of date
            val sdf = java.text.SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss")
            val formattedDate = sdf.format(date)

            if (adapterItem.type == 0) {
                itemView.textViewRowTransactionListType.text = itemView.context.getString(R.string.sell)
                itemView.textViewRowTransactionListType.textColor = itemView.context.resources.getColor(android.R.color.holo_green_dark)
            } else {
                itemView.textViewRowTransactionListType.text = itemView.context.getString(R.string.buy)
                itemView.textViewRowTransactionListType.textColor = itemView.context.resources.getColor(android.R.color.holo_red_dark)
            }

            itemView.textViewRowTransactionListDate.text = formattedDate
            itemView.textViewRowTransactionListPrice.text = "$" + adapterItem.price
            itemView.textViewRowTransactionListAmount.text = adapterItem.amount

            itemView.setOnClickListener {
                if (adapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener

                mItemClickListener?.onListClick(position, adapterItem)
            }
        }
    }

}